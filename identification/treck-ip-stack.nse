local ipOps = require "ipOps"
local nmap = require "nmap"
local packet = require "packet"
local stdnse = require "stdnse"
local openssl = stdnse.silent_require "openssl"

description = [[
The Treck IP Stack is used by a multitude of devices and has a number of potential vulnerabilities that might not be patchable on some devices.  The extent of the usage in various vendors has led these vlnerabilities to be classified as 'Ripple 20', akin to the ripple effect across the supply chains.  This script attempts to identify these devices by use of ICMP probing.  To do this we send an ICMP-Type 165 packet to the device.  If we recieve back an ICMP-Type 166 packet then the device is most likely using the Treck IP Stack.

This script does not attempt to check for any of the potential vulnerabilities as it is meant to soley try and identify these devices.

]]

---
-- @see treck-ip-stack.nse
--
-- @usage
-- sudo nmap -sn --script treck-ip-stack <host[s]>
--
-- @output
-- | trek-ip-stack:
-- |   This device appears to be using the Treck IP Stack.
--
-- @xmloutput
-- <elem key="Trek IP stack">true</elem>
--
--

author = "David Rose"
license = "Same as Nmap--See https://nmap.org/book/man-legal.html"
categories = {"safe", "discovery"}

---
-- Ensure we are running as root and with IPv4, not IPv6
---
hostrule = function(host)
   if(not(nmap.is_privileged())) then
      stdnse.debug1("insufficient privileges to run")
      return false
   end
   if(not(host.mac_addr)) then
      stdnse.debug1("Missing host mac address")
      return false
   end
   return (nmap.is_privileged and host.mac_addr ~= nil)
end

---
-- Build our ICMP-Type 165 packet
-- build_ether_frame requires src and dest mac address.  Thus the requirement on remote mac address
---
local icmp_165_packet = function(iface, host)
   local icmp_pckt = packet.Frame:new()
   icmp_pckt.mac_src = iface.mac
   icmp_pckt.mac_dst = host.mac_addr
   icmp_pckt.ip_bin_src = ipOps.ip_to_str(iface.address)
   icmp_pckt.ip_bin_dst = ipOps.ip_to_str(host.ip)
   icmp_pckt.echo_id = 0x1234
   icmp_pckt.echo_seq = 6
   icmp_pckt.echo_data = "Nmap host discovery"
   icmp_pckt:build_icmp_echo_request()
   icmp_pckt:build_icmp_header(165, 0)
   icmp_pckt:build_ip_packet()
   icmp_pckt:build_ether_frame()
   return icmp_pckt
end

---
-- Probe the system for Treck TCP/IP stack
---
local treck_probe = function(ifname, host)
   local iface = nmap.get_interface_info(ifname)
   local icmp_pckt = icmp_165_packet(iface, host)
   local icmp_pong = nmap.new_socket()
   local icmp_ping = nmap.new_dnet()
   
   icmp_pong:set_timeout(1000)
   icmp_pong:pcap_open(iface.device, 100, false, "icmp and icmp[icmptype]==166")
   
   icmp_ping:ethernet_open(iface.device)
   icmp_ping:ethernet_send(icmp_pckt.frame_buf)

   local status = icmp_pong:pcap_receive()
   icmp_ping:ethernet_close()
   return status
end

action = function(host)
   local interface = nmap.get_interface() or host.interface
   if(not(interface)) then
      return stdnse.format_output(false, "Failed to determine network interface name")
   end
   local itable = nmap.get_interface_info(interface)
   
   if (treck_probe(interface, host)) then
      return ("\n    This device appears to be using the Treck IP Stack.")
   end
end
